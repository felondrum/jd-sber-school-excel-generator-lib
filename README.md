<h2>Библиотека генерации отчетов Excel</h2>

Библиотека позволяет быстро конфигурировать и создавать файл .xlsx с таблицей и возможностью построить график.

 <h3>Конфигурация</h3>
 
 <h5>Инициализация<h5>
 * DTO
    
    
    ExcelGenerator excelGenerator = new ExcelGenerator(YourListOfDto, YourDto.class);
 
         
 
 * JSON в качестве строки
        
        
    ExcelGenerator excelGenerator = new ExcelGenerator(jsonAsString);
 
 <h5> Параметры </h5>
 
  * Указать имя книги (по умолчанию имя DTO или "Free_format" для JSON)
        
        
        excelGenerator.setWorkbookName("WORKBOOK_NAME_AS_STRING");
        
  * Указать имя листа (по умолчанию текущее время в формате "yyyyMMdd_HHmmss"):
  
  
        excelGenerator.setSheet("WORKSHEET_NAME_AS_STRING"); 
        
        
  * Указать путь сохранения файла (для передачи в виде ByteArray необязательно, по умолчанию корневая папка проекта):
  
        
        excelGenerator.setWorkbookPath("ANY_ABSOLUTE_PASS_AS_STRING"); 
        
  * Задать необходимость транспонировать таблицу по полю (по умолчанию отключено):
  
        
        excelGenerator.setFormatToVertical("NAME_OF_FIELD_IN_DTO_OR_JSON_AS_STRING");                
 
 
  * Задать необходимость построить график:
  
  
        excelGenerator.setChart(ExcelChartEnum.BAR, "MAIN_FIELD_NAME", "FIELD_NAME_FOR_SECON_AXIS");  
        
  Первый и второй аргумент обязательны для заполнения. Аргумент 1 - тип необходимого графика (ExcelChartEnum), 2 - имя ключевого поля.
  Аргумент 3 при передаче пустой строкой ("") - выберет все поля, кроме ключевого для построения графика, при указании конкретного поля, будет выбрано только указанное поле. 
  Виды графиков: BAR, COL, AREA, PIE, LINE, SCATTER.
  
  * Задать необходимость построить график в 3D(доступно только для BAR, COL, AREA, PIE, LINE):
  
  
        excelGenerator.setChart3d(ExcelChart3d.ON);
    
  * Задать размер графика (пример по умолчанию):
  
  
        excelGenerator.setChartSize(ExcelChartSizeEnum.MEDIUM);
        
  Варианты для выбора:
        
          
        ExcelChartSizeEnum.SMALL
        ExcelChartSizeEnum.LARGE
        
        
     
  * Задать позиционирование графика относительно таблицы:
    
  
       /** Под таблицей */ excelGenerator.setChartPosition(ExcelChartPositionEnum.BOTTOM);
                      
       /** Справа от таблицы */ excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);
                
                  
                           
        
 <h5>Создать файл в директории</h5>
 
        excelGenerator.exportToFile();
 
 
 <h5>Создать файл в виде ByteArray</h5>
 
 
        excelGenerator.exportToDownload();
        
        
        
 <h3> Пример использования в @RestController</h3> 
 
 Результат - скачивание файла через браузер при вызове Get-метода:
 
       @GetMapping("/download/all_users")
         public ResponseEntity<ByteArrayResource> downloadAllUsers() throws SQLException, IOException {
             try {
                 MediaType mediaType = MediaTypeUtils.getMediaTypeForFileName(this.servletContext, "a.xlsx");
                 List<UserDto> list = userService.getAll();
                 ExcelGenerator excelGenerator = new ExcelGenerator(list, UserDto.class);
                 excelGenerator.setWorkbookName("users_download");
                 excelGenerator.setWorksheetName("report");
                 excelGenerator.setChart(ExcelChartEnum.BAR, "name", "salary");
                 excelGenerator.setChartSize(ExcelChartSizeEnum.LARGE);
                 excelGenerator.setChartPosition(ExcelChartPositionEnum.RIGHT);
                 byte[] data = excelGenerator.exportToDownload();
                 ByteArrayResource resource = new ByteArrayResource(data);
                 String attachments = String.format("attachment;filename=%s.xlsx", excelGenerator.getWorkbookName());
     
                 return  ResponseEntity.ok()
                         .header(HttpHeaders.CONTENT_DISPOSITION,  attachments )
                         .contentType(mediaType)
                         .contentLength(data.length)
                         .body(resource);
             } catch (Exception e) {
                 throw new ExcelProblemException(e.getMessage());
             }
     
         }   
         
         
<h2> Приятного использования! </h2>            