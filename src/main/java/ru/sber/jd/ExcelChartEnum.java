package ru.sber.jd;

public enum ExcelChartEnum {
    NONE,
    PIE,
    LINE,
    BAR,
    COL,
    AREA,
    SCATTER
}
