package ru.sber.jd;

import lombok.Data;
import lombok.val;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/** @author Anton Filippov
 * @version 1.0*/
@Data
public class ExcelGenerator {

    private List<?> listDto;
    private Class<?> classTypeDto;
    private final XSSFWorkbook workbook;
    private  XSSFSheet sheet;
    private ExcelWorksheetFormatEnum format;
    private Integer maxRowCount;
    private String keyColumnName = null;
    private List<String> headers;
    private List<String> verticalHeaders;
    private String workbookName;
    private String worksheetName;
    private String workbookPath = "";
    private JSONArray jsonArray;
    private ExcelSourceEnum source;
    private ExcelChartEnum chart;
    private ExcelChartPositionEnum chartPosition;
    private ExcelChartSizeEnum chartSize;
    private String chartName = "";
    private String horizontalTableChartHeaderColumn = "";
    private String horizontalTableChartDataColumn = "";
    private String verticalTableChartHeaderColumn = "";
    private String verticalTableChartDataColumn = "";
    private ExcelChart3d chart3d = ExcelChart3d.OFF;

    public ExcelGenerator(String jsonAsString) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
        LocalDateTime now = LocalDateTime.now();
        this.jsonArray = new JSONArray(jsonAsString);
        this.workbook = new XSSFWorkbook();
        this.format = ExcelWorksheetFormatEnum.STANDARD;
        this.maxRowCount = jsonArray.length();
        this.worksheetName = dtf.format(now);
        this.workbookName = "Free_format";
        this.source = ExcelSourceEnum.JSON_AS_STRING;
        this.headers = Arrays.asList(JSONObject.getNames(jsonArray.getJSONObject(0)));
        this.chart = ExcelChartEnum.NONE;
        this.chartPosition = ExcelChartPositionEnum.RIGHT;
        this.chartSize = ExcelChartSizeEnum.MEDIUM;
    }

    public ExcelGenerator(List<?> listDto, Class<?> classTypeDto) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss");
        LocalDateTime now = LocalDateTime.now();
        this.workbookName = DtoParseUtil.getShortDtoName(classTypeDto);
        this.worksheetName = dtf.format(now);
        this.listDto = listDto;
        this.classTypeDto = classTypeDto;
        this.workbook = new XSSFWorkbook();
        this.format = ExcelWorksheetFormatEnum.STANDARD;
        this.maxRowCount = listDto.size();
        this.headers = DtoParseUtil.getHeadersList(classTypeDto);
        this.source = ExcelSourceEnum.DTO;
        this.chart = ExcelChartEnum.NONE;
        this.chartPosition = ExcelChartPositionEnum.RIGHT;
        this.chartSize = ExcelChartSizeEnum.MEDIUM;

    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    public void setChart3d(ExcelChart3d chart3d) {
        this.chart3d = chart3d;
    }

    public void setChart(ExcelChartEnum chart, String headerField, String keyField) {
        this.chart = chart;
        this.horizontalTableChartDataColumn = keyField;
        this.horizontalTableChartHeaderColumn = headerField;
        this.verticalTableChartDataColumn = keyField;
        this.verticalTableChartHeaderColumn = keyColumnName;
    }

    private void remakeHeadersList() {
        if (format == ExcelWorksheetFormatEnum.STANDARD && !horizontalTableChartHeaderColumn.equals("")) {
            List<String> headers_copy = new ArrayList<>();
            headers_copy.add(horizontalTableChartHeaderColumn);
            for(String header:headers) {
                if (!header.equals(horizontalTableChartHeaderColumn)) {
                    headers_copy.add(header);
                }
            }
            this.headers = headers_copy;
        }
    }

    public void setWorkbookPath(String workbookPath) {
        this.workbookPath = workbookPath;
    }

    public void setSheet(String worksheetName) {
        this.worksheetName = worksheetName;
        this.sheet = workbook.createSheet(worksheetName);
    }

    public void setSheet() {
        this.sheet = workbook.createSheet(worksheetName);
    }

    public void setFormatToVertical(String keyColumnName) {
        this.format = ExcelWorksheetFormatEnum.VERTICAL;
        this.keyColumnName = keyColumnName;
        if (chart != ExcelChartEnum.NONE) {
            this.verticalTableChartDataColumn = horizontalTableChartDataColumn;
            this.verticalTableChartHeaderColumn = keyColumnName;
        }
    }

    public void setFormatToHorizontal() {
        this.keyColumnName = "";
        this.format = ExcelWorksheetFormatEnum.STANDARD;
        if (chart != ExcelChartEnum.NONE) {
            this.horizontalTableChartDataColumn = verticalTableChartDataColumn;
            this.horizontalTableChartHeaderColumn = verticalTableChartHeaderColumn;
            remakeHeadersList();
        }

    }

    public void setWorkbookName(String workbookName) {
        this.workbookName = workbookName;
    }


    private void writeHeaderRowHorizontalFormat() {
        if (sheet == null) setSheet();
        Row row;
        int columnCount = 1;
        int rowCount = 1;
        row = sheet.createRow(0);
        List<String> baseHeaders = headers;
        if (source == ExcelSourceEnum.DTO) {
            this.headers = DtoParseUtil.getHeadersListHorizontal(listDto, classTypeDto, keyColumnName);
        } else {
            this.headers = DtoParseUtil.getHeadersListHorizontalJson(jsonArray, keyColumnName);
        }
        for(String header:headers) {
            row.createCell(columnCount).setCellValue(header);
            columnCount++;
        }
        this.verticalHeaders = DtoParseUtil.getHeadersListVertical(baseHeaders, keyColumnName);
        for(String header:verticalHeaders) {
            row = sheet.createRow(rowCount);
            row.createCell(0).setCellValue(header);
            rowCount++;
        }
    }


    private void writeHeaderRow() {
        if (sheet == null) setSheet();
        Row row;
        int columnCount = 0;
        row = sheet.createRow(0);
        for (String header : headers) {
            row.createCell(columnCount).setCellValue(header);
            columnCount++;
        }
    }

    private void writeDataRowsHorizontalFormat() {
        Row row;
        int srchCol = 1;
        if (source == ExcelSourceEnum.DTO) {
            for (val dto : listDto) {
                String dtoString = dto.toString();
                Map<String, String> valuesMap = DtoParseUtil.getValuesMap(dtoString, classTypeDto);
                Map<String, CellType> typesMap = DtoParseUtil.getCellTypes(classTypeDto);
                int srchRow = 1;
                for (String header : verticalHeaders) {
                    if (!header.equals(keyColumnName)) {
                        String fieldValue = valuesMap.get(header);
                        if (fieldValue.equals("null")) fieldValue = "";
                        CellType fieldType = typesMap.get(header);
                        row = sheet.getRow(srchRow);
                        if (fieldType.equals(CellType.NUMERIC) && !fieldValue.equals("")) {
                            row.createCell(srchCol, fieldType).setCellValue(Double.parseDouble(fieldValue));
                        } else {
                            row.createCell(srchCol, fieldType).setCellValue(fieldValue);
                        }
                        srchRow++;
                    }
                }
                srchCol++;
            }
        } else {
            for (int i = 0; i < jsonArray.length(); i++) {
                Map<String, String> valuesMap = DtoParseUtil.getValuesMapJson(jsonArray.getJSONObject(i));
                Map<String, CellType> typesMap = DtoParseUtil.getCellTypesJson(jsonArray.getJSONObject(i));
                int srchRow = 1;
                for (String header : verticalHeaders) {
                    if (!header.equals(keyColumnName)) {
                        String fieldValue = valuesMap.get(header);
                        if (fieldValue.equals("null")) fieldValue = "";
                        CellType fieldType = typesMap.get(header);
                        row = sheet.getRow(srchRow);
                        if (fieldType.equals(CellType.NUMERIC) && !fieldValue.equals("")) {
                            row.createCell(srchCol, fieldType).setCellValue(Double.parseDouble(fieldValue));
                        } else {
                            row.createCell(srchCol, fieldType).setCellValue(fieldValue);
                        }
                        srchRow++;
                    }
                }
                srchCol++;
            }
        }
    }

    private void writeDataRows() {
        Row row;
        int rowCount = 1;
        if (source == ExcelSourceEnum.DTO) {
            for (val dto : listDto) {
                int columnCount = 0;
                String dtoString = dto.toString();
                Map<String, String> valuesMap = DtoParseUtil.getValuesMap(dtoString, classTypeDto);
                Map<String, CellType> typesMap = DtoParseUtil.getCellTypes(classTypeDto);
                row = sheet.createRow(rowCount);
                for (String header : headers) {
                    String fieldValue = valuesMap.get(header);
                    if (fieldValue.equals("null")) fieldValue = "";
                    CellType fieldType = typesMap.get(header);
                    if (fieldType.equals(CellType.NUMERIC) && !fieldValue.equals("")) {
                        row.createCell(columnCount, fieldType).setCellValue(Double.parseDouble(fieldValue));
                    } else {
                        row.createCell(columnCount, fieldType).setCellValue(fieldValue);
                    }
                    columnCount++;
                }
                rowCount++;
                if (rowCount > maxRowCount) break;
            }
        } else {
            for (int i = 0; i < jsonArray.length(); i++) {
                int columnCount = 0;
                Map<String, String> valuesMap = DtoParseUtil.getValuesMapJson(jsonArray.getJSONObject(i));
                Map<String, CellType> typesMap = DtoParseUtil.getCellTypesJson(jsonArray.getJSONObject(i));
                row = sheet.createRow(rowCount);
                for (String header : headers) {
                    String fieldValue = valuesMap.get(header);
                    if (fieldValue.equals("null")) fieldValue = "";
                    CellType fieldType = typesMap.get(header);
                    if (fieldType.equals(CellType.NUMERIC) && !fieldValue.equals("")) {
                        row.createCell(columnCount, fieldType).setCellValue(Double.parseDouble(fieldValue));
                    } else {
                        row.createCell(columnCount, fieldType).setCellValue(fieldValue);
                    }

                    columnCount++;
                }
                rowCount++;
                if (rowCount > maxRowCount) break;
            }
        }


    }

    public void prepareMainSheet() {
        if (format == ExcelWorksheetFormatEnum.VERTICAL) {
            writeHeaderRowHorizontalFormat();
            writeDataRowsHorizontalFormat();
        } else if (format == ExcelWorksheetFormatEnum.STANDARD) {
            writeHeaderRow();
            writeDataRows();
        }
        if (chart == ExcelChartEnum.PIE) {drawPie();} else {drawLine();}
    }

    private Integer[] getChartSize() {
        Integer[] result = new Integer[4];
        result[0] = 0;
        result[1] = 10;
        result[2] = 0;
        result[3] = 15;
        if (chartSize == ExcelChartSizeEnum.SMALL) {
            result[1] = 6;
            result[3] = 9;
        } else if (chartSize == ExcelChartSizeEnum.LARGE) {
            result[1] = 16;
            result[3] = 23;
        }
        return result;
    }

    private XSSFClientAnchor makeAnchorForChart(XSSFDrawing drawing) {
        Integer[] size = getChartSize();
        int col1 = 0, row1 = 0, col2 = 0, row2 = 0;
        if (chartPosition == ExcelChartPositionEnum.RIGHT) {
            col1 = headers.size() + size[0];
            row1 = size[2];
            col2 = headers.size() + size[1];
            row2 = size[3];
        } else if (chartPosition == ExcelChartPositionEnum.BOTTOM) {
            Integer rowBound = format == ExcelWorksheetFormatEnum.VERTICAL ? verticalHeaders.size() : maxRowCount;
            col1 = size[0];
            row1 = rowBound + 1;
            col2 = size[1];
            row2 = rowBound + size[3] + 3;
        }
        return drawing.createAnchor(0, 0, 0, 0, col1, row1, col2, row2);
    }

    private CellRangeAddress getHeadersCellAddress() {
        int firstRow = 0;
        int lastRow = 0;
        int firstCol = 0;
        int lastCol = 0;
        if (format == ExcelWorksheetFormatEnum.STANDARD) {
            firstRow = 1;
            lastRow = maxRowCount;
            for (int i = 0; i<headers.size(); i++) {
                if (headers.get(i).equalsIgnoreCase(horizontalTableChartHeaderColumn)) {
                    firstCol = i;
                    lastCol = firstCol;
                    break;
                }
            }
        } else {
            firstCol = 1;
            lastCol = headers.size();
        }
        return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
    }

    private CellRangeAddress getDataCellAddress() {
        int firstRow = 0;
        int lastRow = 0;
        int firstCol = 0;
        int lastCol = 0;
        if (format == ExcelWorksheetFormatEnum.STANDARD) {
            firstRow = 1;
            lastRow = maxRowCount;
            for (int i = 0; i < headers.size(); i++) {
                if (headers.get(i).equalsIgnoreCase(horizontalTableChartDataColumn)) {
                    firstCol = i;
                    lastCol = i;
                    break;
                }
            }

        } else {
            for (int i = 0; i < verticalHeaders.size(); i++) {
                if (verticalHeaders.get(i).equalsIgnoreCase(verticalTableChartDataColumn)) {
                    firstRow = i + 1;
                    lastRow = i + 1;
                    firstCol = 1;
                    lastCol = headers.size();
                    break;
                }
            }
        }
        return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
    }

    private CellRangeAddress getDataCellAddress(String headerName) {
        int firstRow = 0;
        int lastRow = 0;
        int firstCol = 0;
        int lastCol = 0;
        if (format == ExcelWorksheetFormatEnum.STANDARD) {
            firstRow = 1;
            lastRow = maxRowCount;
            for (int i = 0; i < headers.size(); i++) {
                if (headers.get(i).equalsIgnoreCase(headerName)) {
                    firstCol = i;
                    lastCol = i;
                    break;
                }
            }

        } else {
            for (int i = 0; i < verticalHeaders.size(); i++) {
                if (verticalHeaders.get(i).equalsIgnoreCase(headerName)) {
                    firstRow = i + 1;
                    lastRow = i + 1;
                    firstCol = 1;
                    lastCol = headers.size();
                    break;
                }
            }
        }
        return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
    }


    private void drawPie() {
        //create chart on sheet
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        //position of chart on sheet
        XSSFClientAnchor anchor = makeAnchorForChart(drawing);
        XSSFChart chart = drawing.createChart(anchor);
        //make some title settings
        if (chartName.equals("")) {chartName = workbookName;}
        chart.setTitleText(chartName);
        chart.setTitleOverlay(false);
        //make some legend settings
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);
        //no axis configure for pie or pie3d
        //headers for chart

        XDDFDataSource<String> headersOutcomes = XDDFDataSourcesFactory.fromStringCellRange(sheet, getHeadersCellAddress());
        //data for chart
        XDDFNumericalDataSource<Double> values = XDDFDataSourcesFactory.fromNumericCellRange(sheet, getDataCellAddress());
        //prepare data
        ChartTypes chartTypes = chart3d.equals(ExcelChart3d.ON)? ChartTypes.PIE3D: ChartTypes.PIE;
        XDDFChartData data = chart.createData(chartTypes, null, null);
        //some configs for chart
        chart.displayBlanksAs(null);
        data.setVaryColors(true);
        XDDFChartData.Series series = data.addSeries(headersOutcomes, values);
        series.setTitle("SERIES", null);
        chart.plot(data);
    }



    private void drawLine() {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = makeAnchorForChart(drawing);
        XSSFChart chart = drawing.createChart(anchor);
        if (chartName.equals("")) {chartName = workbookName;}
        chart.setTitleText(chartName);
        chart.setTitleOverlay(false);

        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.TOP_RIGHT);
        XDDFDataSource<String> headersOutcomes = XDDFDataSourcesFactory.fromStringCellRange(sheet, getHeadersCellAddress());

        String keyColumn = format.equals(ExcelWorksheetFormatEnum.STANDARD)? horizontalTableChartDataColumn:verticalTableChartDataColumn;

        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        String bottomAxisName = format.equals(ExcelWorksheetFormatEnum.STANDARD)? horizontalTableChartHeaderColumn:verticalTableChartHeaderColumn;

        bottomAxis.setTitle(bottomAxisName);
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        String leftAxisName = keyColumn;
        leftAxis.setTitle(leftAxisName);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        XDDFChartData data = null;
        XDDFNumericalDataSource<Double> values;
        XDDFChartData.Series series;
        ChartTypes chartTypes = ChartTypes.LINE;
        if (this.chart == ExcelChartEnum.LINE) {chartTypes = chart3d.equals(ExcelChart3d.ON)? ChartTypes.LINE3D: ChartTypes.LINE;}
        if (this.chart == ExcelChartEnum.BAR) {chartTypes = chart3d.equals(ExcelChart3d.ON)? ChartTypes.BAR3D: ChartTypes.BAR;}
        if (this.chart == ExcelChartEnum.COL) {chartTypes = chart3d.equals(ExcelChart3d.ON)? ChartTypes.BAR3D: ChartTypes.BAR;}
        if (this.chart == ExcelChartEnum.SCATTER) chartTypes = ChartTypes.SCATTER;
        if (this.chart == ExcelChartEnum.AREA) {chartTypes = chart3d.equals(ExcelChart3d.ON)? ChartTypes.AREA3D: ChartTypes.AREA;}
        if (!keyColumn.equals("")) {
            data = chart.createData(chartTypes, bottomAxis, leftAxis);
            values = XDDFDataSourcesFactory.fromNumericCellRange(sheet, getDataCellAddress());
            series = data.addSeries(headersOutcomes, values);
            series.setTitle(keyColumn, null);
            chart.displayBlanksAs(null);
            data.setVaryColors(true);
            chart.plot(data);
        } else {
            if  (format.equals(ExcelWorksheetFormatEnum.STANDARD)) {
                for (String header:headers) {
                    if (!horizontalTableChartHeaderColumn.equals(header)) {
                        data = chart.createData(chartTypes, bottomAxis, leftAxis);
                        values = XDDFDataSourcesFactory.fromNumericCellRange(sheet, getDataCellAddress(header));
                        series = data.addSeries(headersOutcomes, values);
                        series.setTitle(header, null);
                        chart.plot(data);
                    }
                }
            } else {
                for (String header:verticalHeaders) {
                    data = chart.createData(chartTypes, bottomAxis, leftAxis);
                    values = XDDFDataSourcesFactory.fromNumericCellRange(sheet, getDataCellAddress(header));
                    series = data.addSeries(headersOutcomes, values);
                    series.setTitle(header, null);
                    chart.plot(data);
                }
            }
        }

        if (this.chart.equals(ExcelChartEnum.BAR) || this.chart.equals(ExcelChartEnum.COL)) {
            if (chart3d.equals(ExcelChart3d.ON)) {
                XDDFBar3DChartData bar3d = (XDDFBar3DChartData) data;
                if (this.chart.equals(ExcelChartEnum.COL)) {
                    bar3d.setBarDirection(BarDirection.COL);
                }
                if (this.chart.equals(ExcelChartEnum.BAR)) {
                    bar3d.setBarDirection(BarDirection.BAR);
                }
            } else {
                XDDFBarChartData bar = (XDDFBarChartData) data;
                if (this.chart.equals(ExcelChartEnum.COL)) {
                    bar.setBarDirection(BarDirection.COL);
                }
                if (this.chart.equals(ExcelChartEnum.BAR)) {
                    bar.setBarDirection(BarDirection.BAR);
                }
            }
        }

    }


    public byte[] exportToDownload () throws IOException {
        prepareMainSheet();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        workbook.write(byteArrayOutputStream);
        byte[] data = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return data;
    }

    public void exportToFile() {

        String resultPath = workbookName + ".xlsx";
        if (!workbookPath.equals("")) {
            char lastCharInPath = workbookPath.charAt(workbookPath.length()-1);
            if (lastCharInPath == '\\') {
                resultPath = workbookPath  + workbookName + ".xlsx";
            } else {
                resultPath = workbookPath + "\\" + workbookName + ".xlsx";
            }
        }
        try {
            prepareMainSheet();
            FileOutputStream fileOutputStream = new FileOutputStream(resultPath);
            workbook.write(fileOutputStream);
            workbook.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

