package ru.sber.jd;

import lombok.val;
import org.apache.poi.ss.usermodel.CellType;
import org.json.JSONArray;
import org.json.JSONObject;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DtoParseUtil {

    static List<String> getHeadersList(Class<?> classTypeDto) {
        List<String> headersList = new ArrayList<String>();
        Field[] fields = classTypeDto.getDeclaredFields();
        for (Field field:fields) {
            headersList.add(field.getName());
        }
        return headersList;
    }

    static List<String> getHeadersListHorizontal(List<?> listDto, Class<?> classTypeDto, String keyColumn) {
        List<String> horizontalHeaders = new ArrayList<String>();
        for (val dto:listDto) {
            Map<String, String> valuesMap = getValuesMap(dto.toString(), classTypeDto);
            horizontalHeaders.add(valuesMap.get(keyColumn));
        }
        return horizontalHeaders;
    }

    static List<String> getHeadersListHorizontalJson(JSONArray jsonArray, String keyColumn) {
        List<String> horizontalHeaders = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            Map<String, String> valuesMap = getValuesMapJson(jsonArray.getJSONObject(i));
            horizontalHeaders.add(valuesMap.get(keyColumn));
        }
        return horizontalHeaders;
    }

    static List<String> getHeadersListVertical(List<String> headers, String keyColumn) {
        List<String> verticalHeaders = new ArrayList<String>();
        for (String header:headers) {
            if (!header.equals(keyColumn)) {
                verticalHeaders.add(header);
            }
        }
        return verticalHeaders;
    }



    static Map<String, String> getValuesMap(String dto, Class<?> dtoClassType){
        String shortDtoName = getShortDtoName(dtoClassType);
        String replace = dto.replace(shortDtoName, "").replace("(", "").replace(")", "");
        String[] pairs = replace.split(",");
        Map<String, String> valuesMap = new HashMap<String, String>();
        for (int i=0; i<pairs.length; i++) {
            String pair = pairs[i];
            String[] value = pair.split("=");
            valuesMap.put(String.valueOf(value[0]).trim(), String.valueOf(value[1]));
        }
        return valuesMap;
    }

    static Map<String, String> getValuesMapJson(JSONObject jsonObject){
        Map<String, String> valuesMap = new HashMap<String, String>();
        String[] pairs = jsonObject.toString().split(",");
        for (String p:pairs) {
            String[] pair = p.split(":");
            String value = pair[1].replace("{", "").replace("}", "").replace("\"","").trim();
            String key = pair[0].replace("{", "").replace("}", "").replace("\"","").trim();
            valuesMap.put(key, value);
        }

        return valuesMap;
    }

    static Map<String, CellType> getCellTypes(Class<?> dtoClassType) {
        Field[] fields = dtoClassType.getDeclaredFields();
        Map<String, CellType> typesMap = new HashMap<String, CellType>();
        for (Field f:fields) {
            if (f.getType().isAssignableFrom(Integer.class)
                    || f.getType().isAssignableFrom(Double.class)
                    || f.getType().isAssignableFrom(Long.class)
                    || f.getType().isAssignableFrom(Short.class)
                    || f.getType().isAssignableFrom(int.class)
                    || f.getType().isAssignableFrom(short.class)
                    || f.getType().isAssignableFrom(double.class)
                    || f.getType().isAssignableFrom(long.class)
                    || f.getType().isAssignableFrom(BigInteger.class)
                    || f.getType().isAssignableFrom(BigDecimal.class)
                    || f.getType().isAssignableFrom(BigInteger.class))
            {
                typesMap.put(f.getName().trim(), CellType.NUMERIC);
            } else {
                typesMap.put(f.getName().trim(), CellType.STRING);
            }
        }
        return typesMap;
    }

    static Map<String, CellType> getCellTypesJson(JSONObject jsonObject) {

        String[] fields = JSONObject.getNames(jsonObject);
        Map<String, CellType> typesMap = new HashMap<String, CellType>();
        for (String f:fields) {
            try {
                double field = jsonObject.getDouble(f);
                typesMap.put(f.trim(), CellType.NUMERIC);
            } catch (Exception e) {
                typesMap.put(f.trim(), CellType.STRING);
            }
        }
        return typesMap;
    }

    public static String getShortDtoName(Class<?> dtoClassType) {
        String[] fullDtoName = dtoClassType.getName().split("\\.");
        return fullDtoName[fullDtoName.length-1];
    }

}
