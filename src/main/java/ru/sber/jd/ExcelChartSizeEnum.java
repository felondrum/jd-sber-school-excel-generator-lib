package ru.sber.jd;

public enum ExcelChartSizeEnum {
    SMALL,
    MEDIUM,
    LARGE
}
